import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import shell.Command;
import shell.Shell;
import shop.Basket;
import shop.Product;
import shop.WareHouse;

public class Main {

	/** global warehouse */
	private static WareHouse warehouse = new WareHouse();
	/** global basket */
	private static Basket basket = new Basket();
	private static Shell shell = new Shell();

	public static void main(String[] args) {
		if (args.length > 0) { //read from file
			File file = new File(args[0]);
			if (!file.exists()) {
				System.err.println("File " + file + " doesn't exist.");
				return;
			}
			if (!file.isFile()) {
				System.err.println(file + " is not a file.");
				return;
			}
			try {
				shell.setInputStream(new BufferedInputStream(new FileInputStream(file)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return;
			}
		}
		initCommands(); //add shop-related commands
		shell.start();
	}

	private static void initCommands() {
		String chars = "!#%&()*+-,./=?[]{}";
		Command create;
		shell.addCommand(create = new Command("createProduct", "Add products to warehouse.", //
				Arrays.asList("Usage: createProduct <name> <price>", "price as whole number or decimal ", "\t180 -> 1.80€", "\t2.3 -> 2.30€"), ar -> {
					if (ar.length != 2)
						System.err.println("Wrong number of parameter. Type 'help createProduct'.");
					else {
						try {
							String name = ar[0];
							for (char c : name.toCharArray()) {
								if (!(Character.isLetter(c) || Character.isDigit(c) || chars.indexOf(c) >= 0)) {
									System.err.println("Name can only contain letters, digits, and " + Arrays.toString(chars.toCharArray()) + ".");
									return;
								}
							}
							int price = 0;
							if (ar[1].contains(".")) {
								double x = (Double.parseDouble(ar[1]) * 100);
								price = (int) x;
								if (Math.abs(price - x) >= 1) {
									throw new NumberFormatException();
								}
							} else
								price = Integer.parseInt(ar[1]);
							if (price < 1) {
								System.err.println("Price must be a positive number.");
								return;
							}
							Product d = new Product(name, price);
							warehouse.createProduct(d);
						} catch (NumberFormatException e) {
							System.err.println(ar[1] + " is not a number or out of range.");
						}
					}
				}));
		shell.addCommand(new Command("listProducts", "Show all products of the warehouse.", Arrays.asList("Usage: listProducts"), ar -> {
			if (warehouse.getProducts().isEmpty()) {
				System.out.println("No products in warehouse yet.");
				System.out.println("Use 'createProduct' to add some.");
			} else
				warehouse.getProducts().forEach(System.out::println);
		}));
		shell.addCommand(new Command("addProduct", "Add product to basket.", Arrays.asList("Usage: addProduct <name> [<amount>]", "default amount is 1"), ar -> {
			if (ar.length == 0)
				System.err.println("Wrong number of parameter. Type 'help addProduct'.");
			else {
				try {
					String name = ar[0];
					int num = ar.length == 1 ? 1 : Integer.parseInt(ar[1]);
					Product d = warehouse.getProducts().stream().filter(p -> p.name.equalsIgnoreCase(name)).findFirst().orElse(null);
					if (d != null) {
						basket.addProduct(d, num);
					} else {
						System.err.println("Product " + name + " is not present in the warehouse.");
						System.err.println("Type 'listProducts' to see a list of products.");
					}
				} catch (NumberFormatException e) {
					System.err.println(ar[1] + " is not a number.");
				}
			}
		}));
		shell.addCommand(new Command("discount", "Set discount to basket.", Arrays.asList("Usage: discount (<value>€|%)|remove", "example: discount 3.2%"), ar -> {
			if (ar.length < 1) {
				System.err.println("Wrong number of parameter. Type 'help discount'.");
			} else {
				String value = ar[0];
				try {
					if (value.equals("remove")) {
						basket.removeDiscount();
					} else {
						char end = value.charAt(value.length() - 1);
						if (end == '€' || end == '%') {
							double v = Double.parseDouble(value.substring(0, value.length() - 1));
							basket.setDiscount(end == '€', v);
						} else {
							System.err.println("No € or % found.");
						}
					}
				} catch (NumberFormatException e) {
					System.err.println(value.substring(0, value.length() - 1) + " is not a number.");
				}
			}
		}));
		shell.addCommand(new Command("basket", "Show content, discount and total sum of basket.", Arrays.asList("Usage: basket"), ar -> basket.print()));
		shell.addCommand(new Command("saveWarehouse", "Save warehouse to disk.", Arrays.asList("Usage: saveWarehouse"), ar -> {
			File file = new File(WareHouse.FILE);
			try {
				Files.write(file.toPath(), warehouse.getProducts().stream().//
				map(p -> p.name + "|" + p.price).collect(Collectors.toList()));
				System.out.println("Saved warehouse to disk.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}));
		shell.addCommand(new Command("loadWarehouse", "Load warehouse from disk.", Arrays.asList("Usage: loadWarehouse"), ar -> {
			File file = new File(WareHouse.FILE);
			if (!file.exists()) {
				System.err.println("No file found.");
				return;
			}
			try {
				for (String s : Files.readAllLines(file.toPath())) {
					if (s.isEmpty())
						continue;
					String[] r = s.split(Pattern.quote("|"));
					if (r.length != 2) {
						System.err.println(file + " has invalid syntax.");
						return;
					} else {
						create.execute(r);
					}
				}
				System.out.println("Loaded warehouse from disk.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}));
	}

}
