package shell;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Shell {

	private List<Command> commands = new ArrayList<>();
	private InputStream inputStream = System.in;

	public Shell() {
		initCommands(); //add shell-related commands
	}

	public void start() {
		System.out.println("Welcome!");
		System.out.println("Type 'commands' to see all commands.");
		System.out.println("Type 'help <command>' to see how to use a command.");

		Scanner scanner = new Scanner(inputStream);
		boolean fileUsed = inputStream != System.in; //disable the prompt
		if (!fileUsed)
			System.out.print("> ");

		while (scanner.hasNextLine()) {
			String input = scanner.nextLine().trim();
			if (input.isEmpty())
				continue;
			String[] inputs = input.split("\\s+"); //split into commands arguments
			Command cmd = getCommand(inputs[0]);
			if (cmd == null) {
				System.err.println("Unknown command '" + inputs[0] + "'");
				System.err.println("Type 'commands' to get a list of commands.");
			} else {
				cmd.execute(Arrays.copyOfRange(inputs, 1, inputs.length));
			}
			if (!fileUsed)
				System.out.print("> ");
		}

		scanner.close();
		System.out.println("Bye!");
	}

	public void addCommand(Command cmd) {
		if (getCommand(cmd.getName()) != null) {
			System.err.println("Command " + cmd.getName() + " was already added.");
			return;
		}
		commands.add(cmd);
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	private Command getCommand(String name) {
		return commands.stream().filter(c -> c.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	private void initCommands() {
		commands.add(new Command("quit", "Close programm.", Arrays.asList("Usage: quit"), ar -> {
			System.out.println("Bye!");
			System.exit(0);
		}));
		commands.add(new Command("commands", "Show a list of all commands.", Arrays.asList("Usage: commands"), ar -> commands.forEach(c -> System.out.println(c.getName() + " - " + c.getDescription()))));
		commands.add(new Command("help", "Show how to use a command.", Arrays.asList("Usage: help <command>"), ar -> {
			if (ar.length == 0)
				System.err.println("Wrong number of parameter. Type 'help help'.");
			else {
				Command cmd = getCommand(ar[0]);
				if (cmd == null) {
					System.err.println("Unknown command '" + ar[0] + "'");
				} else {
					for (String s : cmd.getUsage())
						System.out.println(s);
				}
			}
		}));
	}

}
