package shell;

import java.util.List;
import java.util.function.Consumer;

public class Command {
	private final String name, description;
	private final List<String> usage;
	private final Consumer<String[]> consumer;

	public Command(String name, String description, List<String> usage, Consumer<String[]> consumer) {
		super();
		this.name = name;
		this.description = description;
		this.usage = usage;
		this.consumer = consumer;
	}

	public void execute(String[] input) {
		consumer.accept(input);
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<String> getUsage() {
		return usage;
	}

}
