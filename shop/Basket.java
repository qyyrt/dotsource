package shop;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Basket {

	/** map of products in the basket with product as key and price as value */
	private final Map<Product, Integer> products = new HashMap<>();
	public final int id;
	/** discount that is applied on the entire basket */
	private Discount discount;

	public Basket() {
		id = WareHouse.nextID++;
	}

	public void addProduct(Product product, int num) {
		if (num < 1) {
			System.err.println("You can't add 0 or a negative amount of products.");
			return;
		}
		Integer current = products.get(product);
		if (current != null) {
			if (current + num < 0) { // check overflow
				System.err.println("You cannot add more of this item.");
				return;
			}
			products.put(product, current + num);
		} else {
			products.put(product, num);
		}
		System.out.println("Added " + num + "x of " + product + " to basket.");
	}

	public void setDiscount(boolean absolute, double value) {
		if (!absolute && value > 100) {
			System.err.println(value + "% is too high. No discount set.");
			return;
		}
		if (value < 0) {
			System.err.println("Discount must be positive number.");
			return;
		}
		if (value == 0) {
			removeDiscount();
			return;
		}
		this.discount = new Discount(absolute, value);
		System.out.println("Added discount " + (absolute ? value + "€" : value + "%") + ".");
	}

	public void removeDiscount() {
		if (this.discount != null)
			System.out.println("Removed discount.");
		else
			System.out.println("There is no discount set");
		this.discount = null;
	}

	public long totalSum() {
		long sum = products.entrySet().stream().mapToLong(e -> ((long) e.getValue()) * e.getKey().price).sum();
		if (discount != null) {
			if (discount.absolute) {
				sum = Math.max(0, sum - (int) (discount.value * 100));
			} else {
				sum -= sum * discount.value / 100.;
			}
		}
		return sum;
	}

	public void print() {
		for (Entry<Product, Integer> e : products.entrySet()) {
			System.out.println(e.getValue() + "x " + e.getKey() + " -> " + (String.format("%.2f", ((long) e.getValue()) * e.getKey().price / 100.) + "€"));
		}
		if (discount != null)
			System.out.println("Discount: " + (discount.absolute ? String.format("%.2f", discount.value) + "€" : discount.value + "%"));
		System.out.println("Total: " + String.format("%.2f", totalSum() / 100.) + "€");
	}
}
