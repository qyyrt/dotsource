package shop;

public class Discount {
	public final boolean absolute;
	public final double value;

	public Discount(boolean absolute, double value) {
		super();
		this.absolute = absolute;
		this.value = value;
	}
}
