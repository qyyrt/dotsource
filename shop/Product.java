package shop;

public class Product {

	/** name of product */
	public final String name;
	public final int id;
	/** price of product in eurocent */
	public final int price;

	public Product(String name, int price) {
		super();
		this.name = name;
		this.price = price;
		this.id = WareHouse.nextID++;
	}

	@Override
	public String toString() {
		return "{" + name + ": " + String.format("%.2f", price / 100.) + "€}";
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Product))
			return false;
		Product other = (Product) obj;
		return id == other.id;
	}

}
