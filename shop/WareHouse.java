package shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WareHouse {

	public static int nextID = 1;
	public static final String FILE = "warehouse.txt";
	/** list of products which are available */
	private List<Product> allProducts = new ArrayList<>();
	/** cached unmodifiable list of products */
	private List<Product> unmodifiable;

	public void createProduct(Product product) {
		if (allProducts.contains(product)) {
			System.err.println(product + " is already added.");
		} else {
			allProducts.add(product);
			unmodifiable = null;
			System.out.println("Added " + product + " to warehouse.");
		}
	}

	public List<Product> getProducts() {
		if (unmodifiable == null)
			unmodifiable = Collections.unmodifiableList(allProducts);
		return unmodifiable;
	}

}
